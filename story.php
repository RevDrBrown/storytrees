<?php 
//
// story.php
// Written by: Dr. Brown
// Handle insertion, modification and
// deletion of stories.
//
require_once 'includes/global.inc.php';

//check to see if they're logged in
if(!isset($_SESSION['logged_in'])) {
	header("Location: login.php");
}

// Database table name to work with.
$table = "stories";
// ID of current record
$recordID = "";
// Data maintained for the record
$data = array(
	"name" => "",
	"text" => ""
);
// Informational message printed at end of page.
$message = "";

//
// Determine which function has been requested.
//
if(isset($_POST['SHOW_RECORD'])) 
	showRecord();
else if(isset($_POST['UPDATE_RECORD']))
	updateRecord();
else if(isset($_POST['DELETE_RECORD'])) 
	deleteStory();
else if(isset($_POST['ADD_RECORD'])) 
	newStory();
//
// showRecord
//
// Show one record using the POST record ID.
//
function showRecord() {
	global $db, $table, $recordID, $data, $message;
	$recordID = $_POST['recordID'];
	$result = $db->select("*",$table,"id = $recordID");
	if ($db->errorCode)
		$message = $db->errorMsg;
	else 
		setFields($result);
}
//
// newStory
//
// Create a story and an associated node.
//
function newStory() {
	global $recordID, $message;
	insertStory();
	if ($recordID != 0) {
		insertNode();
		header("Location: stories.php");
	}
}
//
//
// insertStory
//
// Insert a story using the current POST data.
//
function insertStory() {
	global $db, $recordID, $data, $message;
	getPost();
	//check to see if story name already exists
	$db->select("name","stories","name = '" . $data['name'] . "'");
	if ($db->numRows != 0) {
		$message .= "That story name is already taken.<br/> \n\r";
		$recordID = 0;
	}
	// If no problems, add the record.
	else {
		$recordID = $db->insert($data, "stories");
		if ($db->errorCode) {
			$message = $db->errorMsg;
			$recordID = 0;
		}
		else
			$message = $data['name'] . " successfully added.";
	}
}
//
// insertNode
//
// Insert a node using the current POST data.
//
function insertNode() {
	global $db, $recordID, $data, $message;
	$data['storyID'] = $recordID;
	$data['x'] = "250";
	$data['y'] = "20";
	$recordID = $db->insert($data, "nodes");
	if ($db->errorCode) {
		$recordID = 0;
	}
}
//
// updateRecord
//
// Update a record using the current POST data.
//
function updateRecord() {
	global $db, $table, $recordID, $data, $message;
	getPost();
	$result = $db->update($data,$table,"id = $recordID");
	if ($db->errorCode)
		$message = $db->errorMsg;
	else
		$message = "Record successfully updated.";
}
//
// deleteStory
//
// Delete the current record.
//
function deleteStory() {
	global $db, $table, $recordID, $message;
	$db->select("name","nodes","storyID = $recordID");
	if ($db->numRows != 1) {
		$message = "Story has nodes. Delete them first.";
		return;
	}
	$recordID = $_POST["recordID"];
	$result = $db->delete($table,"id = $recordID");
	if ($db->errorCode)
		$message = $db->errorMsg;
	else {
		$result = $db->delete("nodes","storyID = $recordID");
		$message = "Record successfully deleted.";
		$recordID = "";
	}
}
//
// getPost
//
//	Load the $data structure with the current POST data.
//
function getPost() {
	global $db, $recordID, $data;
	$recordID = $_POST['recordID'];
	$data['name'] = mysqli_real_escape_string($db->connection, $_POST['name']);
	$data['text'] = mysqli_real_escape_string($db->connection, $_POST['text']);
}
//
// setFields
//
// Set the form fields using the $data structure.
//
function setFields($result) {
	global $data;
	$data['name'] = $result['name'];
	$data['text'] = $result['text'];
}
//
// showCharacter
//
// Displays a single character.
//
function showCharacter($row) {
	echo '<a href="#" class="list-group-item form-control"' .
		' onclick="goEdit(' . $row['id'] . ');">' . 
		$row["name"] . "</a>";
}
//
// showCharacters
//
// Show all of the characters for the story (by calling showCharacter for each).
//
function showCharacters() {
	global $db, $recordID, $message;
	$rows = $db->select("characters.id, name", "storyCharacters, characters",
							  "storyID = $recordID and storyCharacters.characterID = characters.id","name");
	// Check for database errors.
	if ($db->errorCode)
		$message = "An error occurred: " . $db->errorMsg . "\n";
	else {	// no errors
		if ($db->numRows == 0)
			echo "There are no story characters.";
		elseif ($db->numRows == 1) 
			showRecord($rows);
		else {
			foreach($rows as $row) {
				showRecord($row);
			}
		}
	}
}
?>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="Add, update or delete stories." content="">
    <meta name="Dr. Brown" content="">
    <link rel="shortcut icon" href="images/favicon.png">

    <title>Story</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="../../assets/js/html5shiv.js"></script>
      <script src="../../assets/js/respond.min.js"></script>
    <![endif]-->
</head>

<body>
    <?php showNavbar($user);?>
    <div class="container">
        <h2>Story:</h2>
        <form action="story.php" id="storyForm" method="post">
            <?php echo '<input type="hidden" name="recordID" id="recordID" value="' . $recordID . '">'; ?>
            <?php echo '<input type="hidden" name="storyID" id="storyID" value="">'; ?>
            <input type="text" class="form-control" placeholder="Story Name" value="<?php echo $data['name']; ?>" autofocus name="name" required/>
            <br>
            <textarea class="form-control" rows="3" name="text">
                <?php echo $data[ 'text']; ?>
            </textarea>
            <br>			
            <?php
				// Show the ADD button if this is a new record.
				if ($recordID == "")
					echo '<button type="submit" class="btn btn-lg btn-primary btn-block"' . 
						'value="1" name="ADD_RECORD" />Add</button>';
				// If this is an existing record show the UPDATE and DELETE buttons.
				else {
					echo '<button type="submit" class="btn btn-lg btn-primary btn-block"' .
						'value="1" name="UPDATE_RECORD" />Update</button>';
					echo '<button type="submit" class="btn btn-lg btn-primary btn-block"' .
						'value="1" name="DELETE_RECORD" />Delete</button>';
				}
			?>
        </form>
        <?php print $message; ?>
    </div>
</body>
</html>