<?php
//
// treeLayout.php
// Written by: Dr. Brown
//
// Show the tree layout for stories that allows
// the nodes to be created, modified and deleted.
// Users can also select a story path to use to
// create a book with the associated story.
//
require_once 'includes/global.inc.php';

// Check to see if they're logged in
if(!isset($_SESSION['logged_in'])) {
	header("Location: login.php");
}

$message = "";

?>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../assets/ico/favicon.png">

    <style>
        .canvas {
            border: 5px black groove;
            margin: 15px;
        }
        
        .nodeText {
            -webkit-touch-callout: none;
            -webkit-user-select: none;
            -khtml-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }
        
        .node {
            fill-opacity: 0.1;
            stroke-opacity: 1.0;
            stroke-width: 1px;
            stroke: black;
            cursor: pointer;
         }
        
        .nodeHover {
            fill-opacity: 0.5;
        }
        
        .nodeSelected {
            fill-opacity: 0.25;
            stroke-opacity: 1.0;
            stroke-width: 1px;
            stroke: black;
            fill: green;
            cursor: pointer;
        }
        
        .nodeStory {
            fill-opacity: 0.25;
            stroke-opacity: 1.0;
            stroke-width: 1px;
            stroke: black;
            fill: yellow;
            cursor: pointer;
        }
        
        .edge {
            stroke-width: 2;
            stroke: black;
        }
        
        .storyLine {
            stroke-width: 3;
            stroke: green;
        }
    </style>
    <title>Story Trees</title>

    <!-- Bootstrap and FuelUX -->
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">
    <!-- jQuery -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="../../assets/js/html5shiv.js"></script>
      <script src="../../assets/js/respond.min.js"></script>
    <![endif]-->
    <script>
    //
    // Global variables
    //
    var storyID = <?php echo $_POST['storyID']; ?> ;
    var nodeID = <?php echo '"' . $_POST['id'] . '"'; ?> ;
    var userID = <?php echo $userID; ?> ;
    var svgNS = "http://www.w3.org/2000/svg";
    var tree;                   // the JSON-geneated tree object
    var nodeList = [];          // node objects associative array   
    var textSize = 12;          // size of node text
    var padding = 5;            // padding inside of node rectangles
    var selectedNode = null;    // the currenctly selected node object
    var dragNode = null;        // the node object being dragged
    var dragBeginX = 0;         // x location where drag started
    var dragBeginY = 0;         // y location where drag started
    var dragOffsetX = 0;        // x offset where drag click occurred in object
    var dragOffsetY = 0;        // y offset where drag click occurred in object
   // These are shortcuts to document elements
    var treeLayout = null;
    var nodeNameObj = null;
    var nodeTextObj = null;
    var nodeSubtextObj = null;
    var nodeIDObj = null;
    var updateButtonObj = null;
    var newChildButtonObj = null;
    var deleteButtonObj = null;
    var createStoryButtonObj = null;
    var msgOBj = null;
    // Used to create a list of nodes in a story
    var storyPath = "";

    //
    // onLoad
    // Called after body load. Sends AJAX request for
    // JSON tree data and calls parseTree to parse it.
    //
    function onLoad() {
        var xmlhttp = new XMLHttpRequest();

        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                parseTree(xmlhttp.responseText);
            }
        }
        xmlhttp.open("POST", "treeJSON.php", true);
        xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        var data = "storyID=" + <?php echo $_POST['storyID']; ?>;
        xmlhttp.send(data);
    }
    //
    // parseTree
    // 
    function parseTree(response) {
        console.log(response);
        tree = JSON.parse(response);
        init();
    }
    //
    // init
    // Initialize shortcut elements and associative array
    // of node objects.
    //
    function init() {
        treeLayout = document.getElementById("treeLayout");
        nodeNameObj = document.getElementById("name");
        nodeTextObj = document.getElementById("text");
        nodeSubtextObj = document.getElementById("subtext");
        nodeIDObj = document.getElementById("id");
        msgObj = document.getElementById("message");
        updateButtonObj = document.getElementById("updateButton");
        newChildButtonObj = document.getElementById("newChildButton");
        deleteButtonObj = document.getElementById("deleteButton");
        createStoryButtonObj = document.getElementById("createStoryButton");
        // Create an associative array of Nodes
        for (var i = 0; i < tree.nodes.length; i++) {
            var node = tree.nodes[i];
            node.selected = false;
            node.inStory = false;
            nodeList[node.id] = node;
            tree.nodes[i].edgesFrom = [];
            tree.nodes[i].edgesTo = [];
        }
        renderNodes(tree);
        renderEdges(tree);
        if (nodeID != "") 
            selectNode(nodeList[nodeID]);
    }
    //
    // renderEdges
    // Walk through the tree edges and render
    // each of them as SVG lines.
    //
    function renderEdges(tree) {
        // Draw Edges
        for (var i = 0; i < tree.edges.length; i++) {
            var edge = tree.edges[i];
            renderEdge(edge);
        }
    }
    //
    // renderNodes
    // Walk through the tree edges and render
    // each of them as SVG rect's and text.
    //
    function renderNodes(tree) {
        // Draw Nodes
        for (var i = 0; i < tree.nodes.length; i++) {
            var node = tree.nodes[i];
            renderNode(node);
        }
    }

    //
    // renderEdge
    // Render the edges by creating SVG line objects.
    //
    function renderEdge(edge) {
        var fromNode = nodeList[edge.fromNodeID];
        var toNode = nodeList[edge.toNodeID];
        var newLine = document.createElementNS(svgNS, "line");
        edge.lineObj = newLine;
        newLine.setAttributeNS(null, "x1", Number(fromNode.x) +
                               fromNode.svgObj.getBBox().width / 2);
        newLine.setAttributeNS(null, "y1", Number(fromNode.y) +
                               fromNode.svgObj.getBBox().height);
        newLine.setAttributeNS(null, "marker-end", "url(#arrowHead)");
        fromNode.edgesFrom.push(edge);
        newLine.setAttributeNS(null, "x2", Number(toNode.x) + 
                               toNode.svgObj.getBBox().width / 2);
        newLine.setAttributeNS(null, "y2", Number(toNode.y));
        toNode.edgesTo.push(edge);
        newLine.setAttributeNS(null, "class", "edge");
        treeLayout.appendChild(newLine);
    }
    //
    // renderNode
    // Render the nodes with SVG text and rect objects.
    //
    function renderNode(node) {
        var newSVG = document.createElementNS(svgNS, "svg");
        newSVG.setAttributeNS(null, "x", node.x);
        newSVG.setAttributeNS(null, "y", node.y);
        newSVG.setAttributeNS(null, "height", textSize + 2 * padding);

        var newText = document.createElementNS(svgNS, "text");
        newText.setAttributeNS(null, "x", padding);
        newText.setAttributeNS(null, "y", textSize + padding);
        newText.setAttributeNS(null, "class", "nodeText");

        newText.setAttributeNS(null, "font-size", textSize);
        var textNode = document.createTextNode(node.name);
        newText.appendChild(textNode);
        // Hookup event handlers for selection and dragging.
        newSVG.addEventListener("mouseover", function () {
            nodeMouseOver(node)
        });
        newSVG.addEventListener("mouseout", function () {
            nodeMouseOut(node)
        });
        newSVG.addEventListener("mousedown", function () {
            nodeMouseDown(node)
        });
        treeLayout.addEventListener("mouseup", function () {
            nodeMouseUp()
        });
        treeLayout.addEventListener("mousemove", function () {
            nodeMouseMove()
        });

        newSVG.appendChild(newText);
        treeLayout.appendChild(newSVG);
        var textWidth = newText.getComputedTextLength();
        newSVG.setAttributeNS(null, "width", textWidth + 2 * padding);

        var newRect = document.createElementNS(svgNS, "rect");
        newRect.setAttributeNS(null, "x", 0);
        newRect.setAttributeNS(null, "y", 0);
        newRect.setAttributeNS(null, "rx", 10);
        newRect.setAttributeNS(null, "ry", 10);
        newRect.setAttributeNS(null, "width", textWidth + 2 * padding);
        newRect.setAttributeNS(null, "height", textSize + 2 * padding);
        newRect.setAttributeNS(null, "class", "node");
        newSVG.appendChild(newRect);

        node.rectObj = newRect;
        node.textObj = newText;
        node.svgObj = newSVG;
    }
    //
    // setMessage
    // Utility method to set the message field.
    //
    function setMessage(msg) {
        msgObj.innerHTML = msg;
    }
    //
    // nodeMouseOver
    // Called when the user mouses over a node.
    //
    function nodeMouseOver(node) {
        node.rectObj.setAttributeNS(null, "class", "nodeHover");
    }
    //
    // nodeMouseOut
    // Called when the user mouses out of a node.
    //
    function nodeMouseOut(node) {
        if (node.selected)
            node.rectObj.setAttributeNS(null, "class", "nodeSelected");
        else if (node.inStory)
            node.rectObj.setAttributeNS(null, "class", "nodeStory");
        else
            node.rectObj.setAttributeNS(null, "class", "node");
    }
    //
    // nodeMouseDown
    // Called when the user clicks on a node. It saves
    // information needed to support dragging the node.
    //
    function nodeMouseDown(node) {
        setMessage("");
        dragNode = node;
        dragOffsetX = window.event.clientX - node.x;
        dragOffsetY = window.event.clientY - node.y;
        dragBeginX = node.x;
        dragBeginY = node.y;
    }
    //
    // nodeMouseUp
    // Called when the user releases the click on a node. 
    // It handles moving the node (if it was moved).
    //
    function nodeMouseUp() {
        if (dragNode != null) {
            selectNode(dragNode);
            dragNode = null;
            if ((dragBeginX != selectedNode.x) || (dragBeginY != selectedNode.y))
                updateNodeLocation(selectedNode.id, selectedNode.x, selectedNode.y);
        }
    }
    //
    // selectNode
    // Handles node selection and deselection.
    //
    function selectNode(node) {
        if (selectedNode != null) {
            clearStoryPath(selectedNode);
            selectedNode.selected = false;
            nodeMouseOut(selectedNode);
        }
        setStoryPath(node);
        selectedNode = node;
        selectedNode.selected = true;
        nodeMouseOut(selectedNode);
        showData(selectedNode);
        showUpdateButtons();
    }
    //
    // nodeMouseMove
    // This code drags a node around on the screen.
    //
    function nodeMouseMove(node) {
        if (dragNode != null) {
            var x = window.event.clientX - dragOffsetX;
            var y = window.event.clientY - dragOffsetY;
            dragNode.svgObj.setAttributeNS(null, "x", x);
            dragNode.svgObj.setAttributeNS(null, "y", y);
            dragNode.x = x;
            dragNode.y = y;
            for (var i = 0; i < dragNode.edgesFrom.length; i++) {
                var line = dragNode.edgesFrom[i].lineObj;
                line.setAttributeNS(null, "x1", x + 
                                    dragNode.svgObj.getBBox().width / 2);
                line.setAttributeNS(null, "y1", y + 
                                    dragNode.svgObj.getBBox().height);
            }
            for (var i = 0; i < dragNode.edgesTo.length; i++) {
                var line = dragNode.edgesTo[i].lineObj;
                line.setAttributeNS(null, "x2", x + 
                                    dragNode.svgObj.getBBox().width / 2);
                line.setAttributeNS(null, "y2", y);
            }
        }
    }
    //
    // clearStoryPath
    // Clear the story path indicator for nodes in the lineage
    // from the passed node back to the root node of the story.
    // 
    function clearStoryPath(node) {
        // Repeat for the node and all it's all ancestors
        while(node.edgesTo.length != 0) {
            node.inStory = false;
            nodeMouseOut(node);
            var edge = node.edgesTo[0];         // follow edge
            edge.lineObj.setAttributeNS(null, "class", "edge");
            node = nodeList[edge.fromNodeID];   // to parent node
         }
        node.inStory = false;
        nodeMouseOut(node);
    }
    //
    // setStoryPath
    // Set the story path indicator for nodes in the lineage 
    // from the passed noded back through the root node of the story.
    //
    function setStoryPath(node) {
        // Repeat for the node and all it's all ancestors
        storyPath = "";
        while(node.edgesTo.length != 0) {
            if (storyPath == "")
                storyPath = "" + node.id;
            else
                storyPath = "" + node.id + "," + storyPath;
            node.inStory = true;
            nodeMouseOut(node);
            var edge = node.edgesTo[0];         // follow edge
            edge.lineObj.setAttributeNS(null, "class", "storyLine");
            node = nodeList[edge.fromNodeID];   // to parent node
        }
        if (storyPath == "")
            storyPath = "" + node.id;
        else
            storyPath = "" + node.id + "," + storyPath;
        node.inStory = true;
        nodeMouseOut(node);
    }
    //
    // showUpdateButtons
    // Show the buttons when a node is selected.
    //
    function showUpdateButtons() {
        updateButtonObj.style.visibility = 'visible';
        newChildButtonObj.style.visibility = 'visible';
        deleteButtonObj.style.visibility = 'visible';
        createStoryButtonObj.style.visibility = 'visible';
    }
    //
    // hideUpdateButtons
    // Hide the buttons when no node is selected.
    //
    function hideUpdateButtons() {
        updateButtonObj.style.visibility = 'hidden';
        newChildButtonObj.style.visibility = 'hidden';
        deleteButtonObj.style.visibility = 'hidden';
        createStoryButtonObj.style.visibility = 'hidden';
    }
    //
    // showData
    // Displays the data for a node.
    //
    function showData(node) {
        nodeNameObj.value = node.name;
        nodeTextObj.value = node.text;
        nodeSubtextObj.value = node.subtext;
        nodeIDObj.value = node.id;
    }
    //
    // updateNode
    // Update a node (in the database) via an AJAX call.
    //
    function updateNode() {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                setMessage(xmlhttp.responseText);
                var id = nodeIDObj.value;
                var node = nodeList[id];
                node.name = nodeNameObj.value;
                node.text = nodeTextObj.value;
                node.subtext = nodeSubtextObj.value;
            }
        }
        xmlhttp.open("POST", "updateNode.php", false);
        xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        var data = "id=" + encodeURI(nodeIDObj.value) + "&name=" + 
            encodeURI(nodeNameObj.value) +
            "&text=" + encodeURI(nodeTextObj.value) + "&subtext=" + 
            encodeURI(nodeSubtextObj.value);
        xmlhttp.send(data);
    }
    //
    // updateNodeLocaton
    // Update a node's location (in the database) via an AJAX call.
    //
    function updateNodeLocation(id, x, y) {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                setMessage(xmlhttp.responseText);
            }
        }
        xmlhttp.open("POST", "updateNodeLocation.php", false);
        xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        var data = "id=" + nodeIDObj.value + "&x=" + x + "&y=" + y;
        xmlhttp.send(data);
    }
    //
    // newChildNode
    // Create a new child node of the currently selcted node in
    // the database via AJAX.
    //
    function newChildNode() {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                nodeID = xmlhttp.responseText;
                if (nodeID == "0") {
                    setMessage("Couldn't create new record...");
                } else {
                    nodeIDObj.value = nodeID;
                }
            }
        }
        xmlhttp.open("POST", "newChildNode.php", false);
        xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        var y = Number(selectedNode.y) + 30;
        var data = "storyID=" + storyID + "&fromNodeID=" + nodeIDObj.value +
            "&x=" + selectedNode.x + "&y=" + y + "&ownerID=" + userID;
        xmlhttp.send(data);
    }
    //
    // deleteNode
    // Delete a node (in the database) via AJAX.
    //
    function deleteNode() {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                setMessage(xmlhttp.responseText);
                if (xmlhttp.responseText == "Success")
                    document.getElementById("nodeForm").submit();
                else if (xmlhttp.responseText == "Story Deleted")
                    window.location.href = "stories.php";
                else
                    setMessage(xmlhttp.responseText);
            }
        }
        xmlhttp.open("POST", "deleteNode.php", false);
        xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        var data = "id=" + nodeIDObj.value + "&storyID=" + storyID;
        xmlhttp.send(data);
    }
    //
    // changeNodeName
    // Changes the rendered node name. Not currently used. It doesn't
    // adjust the size of the node's rectangle correctly. Names currently
    // need to be updated along with the rest of the node data.
    //
    function changeNodeName(name) {
        //			updateNode();
        //			document.getElementById("nodeForm").submit();
        //			var node = nodeList[nodeIDObj.value];
        //			node.name = name;
        //			node.textObj.innerHTML = name;
        //			var textWidth = node.textObj.getComputedTextLength(); 	
        //			node.rectObj.setAttribute("width",textWidth+2*padding); 
        //			node.rectObj.setAttribute("height",textSize+2*padding); 
    }
    //
    // createStory
    // Called to set up the current path of the story in preparation
    // for going to storyLayout.php.
    //
    function createStory() {
        document.getElementById("nodeIDs").value = storyPath;
    }

</script>
</head>

<body onload="onLoad();">
    <?php showNavbar($user); ?>
    <div class="container-fluid" style="margin-top: 15px">
        <div class="row" width="100%">
            <div class="col-md-6">
                <svg id="treeLayout" height="580" width="100%" class="canvas">
                    <marker id="arrowHead" viewBox="0 0 10 10" refX="0" refY="5" markerUnits="strokeWidth" markerWidth="4" markerHeight="3" orient="auto">
                        <path d="M 0 0 L 10 5 L 0 10 z" />
                    </marker>
                </svg>
                <div class="form-inline">
                    <form id="storyForm" action="storyLayout.php" method="post">
                        <input type="hidden" id="storyID" name="storyID" 
                               value="<?php echo $_POST['storyID']; ?>">
                        <input type="hidden" id="nodeIDs" name="nodeIDs" value="0">
                       <button id="createStoryButton" onclick="createStory(); return(true);" 
                                class="btn btn-default" 
                                style="margin-left: 200px; margin-top: 10px; visibility:hidden">
                            Create Story
                        </button>
                    </form>
                </div>
            </div>
            <div class="col-md-6">
                <form id="nodeForm" action="treeLayout.php" method="post">
                    <input type="hidden" id="storyID" name="storyID" 
                           value="<?php echo $_POST['storyID']; ?>">
                    <input type="hidden" id="id" name="id" value="0">
                    <div width="100%" style="padding: 15px">
                        <div class="form-group">
                            <label for="name">Name:</label>
                            <input type="text" class="form-control" id="name" name="name"
                                   onchange="changeNodeName(this.value);">
                        </div>
                        <div class="form-group">
                            <label for="text">Text:</label>
                            <textarea class="form-control" name="text" id="text" rows="10">
                            </textarea>
                        </div>
                        <div class="form-group">
                            <label for="subtext">Subtext:</label>
                            <textarea class="form-control" name="subtext" id="subtext"
                                      rows="10"></textarea>
                        </div>
                        <div class="form-inline">
                            <button id="updateButton" onclick="updateNode(); return(true);"
                                    class="btn btn-default" 
                                    style="margin-top: 10px; visibility:hidden">
                                Update</button>
                            <button id="newChildButton" onclick="return(newChildNode());" 
                                    class="btn btn-default" 
                                    style="margin-top: 10px; visibility: hidden">
                                New Child</button>
                            <button id="deleteButton" onclick="deleteNode();return(false);" 
                                    class="btn btn-default" 
                                    style="margin-top: 10px; visibility:hidden">
                                Delete</button>
                        </div>
                    </div>
                    <div id="message"></div>
                </form>
            </div>
        </div>
        <?php echo $message; ?>
    </div>
</body>
</html>