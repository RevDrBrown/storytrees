<?php
//
// users.php
// Written by: Dr. Brown
//
// Show the list of users and allow selection for
// modification or deletion. Also allows new users
// to be added. All transfer the user to user.php
// which handles an individual user.
//
require_once 'includes/global.inc.php';

// Check to see if they're logged in
//if(!isset($_SESSION['logged_in'])) {
//	header("Location: login.php");
//}
// Database table name to show.
$table = "users";
$message = "";
//
// showRecord
//
// Displays a single record.
//
function showRecord($row) {
	echo '<a href="#" class="list-group-item form-control"' .
		' onclick="goEdit(' . $row['id'] . ');">' . 
		$row["firstName"] . " " . $row["lastName"] . "</a>";
}
//
// showRecords
//
// Show all of the records (by calling showRecord for each).
//
function showRecords() {
	global $db, $table, $message;
	$rows = $db->select("*", $table,"","lastName");
	// Check for database errors.
	if ($db->errorCode)
		$message = "An error occurred: " . $db->errorMsg . "\n";
	else {	// no errors
		if ($db->numRows == 0)
			echo "There are no $table.";
		elseif ($db->numRows == 1) 
			showRecord($rows);
		else {
			foreach($rows as $row) {
				showRecord($row);
			}
		}
	}
}
?>
<html>

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="shortcut icon" href="../../assets/ico/favicon.png">

	<title>Users</title>

	<!-- Bootstrap core CSS -->
	<link href="css/bootstrap.css" rel="stylesheet">
	<link href="css/custom.css" rel="stylesheet">

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
      <script src="../../assets/js/html5shiv.js"></script>
      <script src="../../assets/js/respond.min.js"></script>
    <![endif]-->
	<script>
		function goEdit(id) {
			document.getElementById("recordID").value = id;
			document.getElementById("myForm").submit();
		}
	</script>
</head>

<body>
	<?php showNavbar($user); ?>
	<div class="container">
		<h2>Users:</h2>
		<form id="myForm" method="POST" action="user.php">
			<input type="hidden" id="recordID" name="recordID">
			<input type="hidden" name="SHOW_RECORD" value="1">
		</form>
		<div class="list-group">
		<?php
			showRecords();
		?>
</div>
<a href="user.php" class="btn btn-lg btn-primary btn-block" value="add" name="add-submit" />New</a>
<?php
	echo $message;
?>
</div>
</body>
<!-- Bootstrap core JavaScript
    ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="//code.jquery.com/jquery-latest.min.js"></script>
<script src="js/bootstrap.js"></script>

</html>