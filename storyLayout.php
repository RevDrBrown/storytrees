<?php
//
// storyLayout.php
// Written by: Dr. Brown
//
// Show the story layout for a story using
// a two page booklet layout that allows page 
// turning.
//
require_once 'includes/global.inc.php';

// Check to see if they're logged in
if(!isset($_SESSION['logged_in'])) {
	header("Location: login.php");
}

$message = "";

?>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../assets/ico/favicon.png">

    <style>
    </style>
    <title>Story Book</title>

    <!-- Bootstrap and FuelUX -->
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">
    <!-- jQuery -->
    <script type="text/javascript" src="js/jquery-1.11.3.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="../../assets/js/html5shiv.js"></script>
      <script src="../../assets/js/respond.min.js"></script>
    <![endif]-->
    <script>
    speechSynthesis.cancel(); // reset module to use native speech
    var storyID = <?php echo $_POST['storyID']; ?> ;
    var nodeIDs = <?php echo '"' . $_POST['nodeIDs'] . '"' ?> ;
    var story = null;
    //
    // onLoad
    // After the body loads, this method is called.
    // It gets the story JSON data via AJAX and then
    // renders the story into divs in a booklet.
    //
    function onLoad() {
        var xmlhttp = new XMLHttpRequest();

        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                console.log(xmlhttp.responseText);
                story = JSON.parse(xmlhttp.responseText);
                parseStory();
            }
        }
        xmlhttp.open("POST", "storyJSON.php", true);
        xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        var data = "storyID=" + storyID + "&nodeIDs=" + nodeIDs;
        xmlhttp.send(data);
    }
    //
    // parseStory
    // Parse the JSON story data and create div pages for
    // each node's text.
    //
    function parseStory() {
        var storyBook = document.getElementById("storyBook");
        var pages = story.pages;
        var numPages = pages.length;
        for(var i = 0; i < numPages; i++) {
            var newDiv = document.createElement("div");
            newDiv.addEventListener("mousedown", function () {
                speak(event.target);
            });           
            newDiv.innerHTML = "<h4>" + pages[i].name + "</h4>" + pages[i].text;
            storyBook.appendChild(newDiv);
        }
        $(function () {		
            //$("#storyBook").booklet({ closed: true, covers: true});
            $("#storyBook").booklet({width: 800});
        });
    }
    //
    // speak
    // Read the book text using speech synthesis.
    //
    function speak(divObj) {
        var msg = new SpeechSynthesisUtterance();
        var voices = window.speechSynthesis.getVoices();
        msg.voice = voices[10]; // Note: some voices don't support altering params
        msg.voiceURI = 'native';
        msg.volume = 1; // 0 to 1
        msg.rate = 1; // 0.1 to 10
        msg.pitch = 2; //0 to 2
        msg.text = divObj.innerText;
        msg.lang = 'en-US';
        speechSynthesis.speak(msg);
    }
</script>
<link href="js/booklet/jquery.booklet.latest.css" type="text/css" 
          rel="stylesheet" media="screen, projection, tv" />
</head>

<body onload="onLoad();">
    <?php showNavbar($user); ?>
    <div class="container-fluid" style="margin-top: 15px">
        <div class="col-md-6">
            <form id="nodeForm" action="treeLayout.php" method="post">
                <input type="hidden" id="storyID" name="storyID" 
                       value="<?php echo $_POST['storyID']; ?>">
                <input type="hidden" id="id" name="id" value="0">
                <section>
                    <div id="storyBook" style="border: 5px ridge black">
                    </div>
                </section>
                <footer></footer>
                <script src="js/booklet/jquery-ui.min.js"></script>
                <script src="js/booklet/jquery.easing.1.3.js"></script>
                <script src="js/booklet/jquery.booklet.latest.js"></script>
                <script>
                </script>
                <div class="form-inline">
                    <form id="treeForm" action="treeLayout.php" method="post">
                        <input type="hidden" id="storyID" name="storyID" 
                               value="<?php echo $_POST['storyID']; ?>">
                        <input type="hidden" id="id" name="id" value="0">
                        <button id="treeLayoutButton" onclick="return(true);" 
                                class="btn btn-default" 
                                style="margin-left: 200px; margin-top: 10px;">
                            Go to Tree Layout
                        </button>
                        <button id="speak" onclick="speechSynthesis.cancel(); return(false);" 
                                class="btn btn-default" 
                                style="margin-top: 10px;">
                            Reset Speech Module
                        </button>
                    </form>
                </div>
        </div>

        <div id="message"></div>
        </form>
    </div>
    </div>
    <?php echo $message; ?>
    </div>
</body>
</html>