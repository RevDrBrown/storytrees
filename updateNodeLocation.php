<?php
//
// updateNode.php
// Written by: Dr. David Brown
//
// Update the location a node via supplied
// $_POST information.
//
require_once 'includes/global.inc.php';

// Check to see if they're logged in
if(!isset($_SESSION['logged_in'])) {
	exit("User not authorized.");
}

//
// updateNode
//
// Update a record using the current POST data.
//
function updateNode() {
	global $db, $recordID, $data, $message;
	getData();
	$result = $db->update($data,"nodes","id = $recordID");
	if ($db->errorCode)
		$message = $db->errorMsg;
	else
		$message = "Tree node location successfully updated.";
}
//
// getData
//
//	Load the $data structure with the current POST data.
//
function getData() {
	global $db, $recordID, $data;
	$recordID = $_POST['id'];
	$data['x'] = mysqli_real_escape_string($db->connection, $_POST['x']);
	$data['y'] = mysqli_real_escape_string($db->connection, $_POST['y']);
}

updateNode();
echo $message;
?>