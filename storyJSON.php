<?php
//
// storyJSON.php
// Written by: Dr. Brown
//
// Returns JSON data for the story as determined
// by the list of nodeIDs passed into the program
// via $_POST data (storyID and nodeIDs).
//
require_once 'includes/global.inc.php';

// Check to see if they're logged in
if(!isset($_SESSION['logged_in'])) {
	header("Location: login.php");
}

$message = "";
$json = "";

//
// showStory
//
// Build the JSON data for the indicated story.
//
function showStory($storyID) {
	global $db, $message, $json;
	$rows = $db->select("*", "stories","id=$storyID");
	// Check for database errors.
	if ($db->errorCode)
		$message = "An error occurred: " . $db->errorMsg . "\n";
	else {	// no errors
		if ($db->numRows == 0) {
			$message = "No stories returned.";
			$json = $json . '"id" : "",'. "\n" .
				'"name" : "",'. "\n" .
				'"text" : "",'. "\n";
		}
		else {
			if ($db->numRows == 1)
				$row = $rows;
			else {
				$message = "Why did we get more than one story with the same ID?!?\n";
				$row = $rows[0];
			}
			$json = $json .  '"id" : "' . $row['id'] . '",'. "\n" .
				'"name" : "' . $row['name'] . '",'. "\n" .	
				'"text" : "' . $row['text'] . '",'. "\n";		
		}
	}
}
//
// showNodes
//
// Show all of the nodes (by calling showNode for each).
//
function showNodes($nodeIDs) {
	global $db, $message, $json;
    $numNodes = count($nodeIDs);
    for($i = 0; $i < $numNodes; $i++) {
        $nodeID = $nodeIDs[$i];
        $rows = $db->select("*", "nodes","id=$nodeID");
        // Check for database errors.
        if ($db->errorCode)
            $message = "An error occurred: " . $db->errorMsg . "\n";
        else {	// no errors
            if ($db->numRows == 0)  
                $message = "No nodes found.";
            elseif ($db->numRows == 1) {
                showNode($rows);
                if ($i != ($numNodes - 1))
                    $json = $json . ",\n";
                else
                    $json = $json . "\n";
            }
            else {
                showNode($rows[0]);
                if ($i != ($numNodes - 1))
                    $json = $json . ",\n";
                else
                    $json = $json . "\n";
            }
        }
    }
}
//
// showNode
//
// Build the JSON for a single node.
//
function showNode($row) {
    global $json;
	$json = $json .  '{"id" : "' . $row['id'] . '",'. "\n" .
		'"name" : "' . $row['name'] . '",'. "\n" .
		'"text" : "' . $row['text'] . '",'. "\n" .
		'"subtext" : "' . $row['subtext'] . '"}';
}
//
// Direct the building of the JSON
// data for the story. The nodeIDs are
// a comma-separated list.
//
$storyID = $_POST['storyID'];
$nodeIDs = explode(",",$_POST['nodeIDs']);
$json = $json .  "{";
showStory($storyID);
$json = $json .  '"pages" : [' . "\n";
showNodes($nodeIDs);
$json = $json .  "]\n";
$json = $json .  "}";
echo str_replace("\\'", "'", $json);
?>
