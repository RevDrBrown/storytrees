<?php
//
// updateNode.php
// Written by: Dr. David Brown
//
// Update the data in a node via supplied
// $_POST information.
//
require_once 'includes/global.inc.php';

// Check to see if they're logged in
if(!isset($_SESSION['logged_in'])) {
	exit("User not authorized.");
}

//
// updateNode
//
// Update a record using the current POST data.
//
function updateNode() {
	global $db, $recordID, $data, $message;
	getPost();
	$result = $db->update($data,"nodes","id = $recordID");
	if ($db->errorCode)
		$message = $db->errorMsg;
	else
		$message = "Record successfully updated.";
}

//
// getPost
//
//	Load the $data structure with the current POST data.
//
function getPost() {
	global $db, $recordID, $data;
	$recordID = $_POST['id'];
	$data['name'] = mysqli_real_escape_string($db->connection,$_POST['name']);
	$data['text'] = mysqli_real_escape_string($db->connection,$_POST['text']);
	$data['subtext'] = mysqli_real_escape_string($db->connection,$_POST['subtext']);
}
updateNode();
echo $message;
?>