<?php
//
// treeJSON.php
// Written by: Dr. Brown
//
// Returns JSON data for the entire story tree
// that includes all nodes and their connected
// edges.
//
require_once 'includes/global.inc.php';

// Check to see if they're logged in
if(!isset($_SESSION['logged_in'])) {
	header("Location: login.php");
}

$message = "";
$json = "";

//
// showStory
//
// Build the JSON data for the indicated story.
//
function showStory($storyID) {
	global $db, $message, $json;
	$rows = $db->select("*", "stories","id=$storyID");
	// Check for database errors.
	if ($db->errorCode)
		$message = "An error occurred: " . $db->errorMsg . "\n";
	else {	// no errors
		if ($db->numRows == 0) {
			$message = "No stories returned.";
			$json = $json . '"id" : "",'. "\n" .
				'"name" : "",'. "\n" .
				'"text" : "",'. "\n" .
				'"startNodeID" : "",'. "\n" .
				'"endNodeID" : "",'. "\n";
		}
		else {
			if ($db->numRows == 1)
				$row = $rows;
			else {
				$message = "Why did we get more than one story with the same ID?!?\n";
				$row = $rows[0];
			}
			$json = $json .  '"id" : "' . $row['id'] . '",'. "\n" .
				'"name" : "' . $row['name'] . '",'. "\n" .	
				'"text" : "' . $row['text'] . '",'. "\n" .	
				'"startNodeID" : "' . $row['startNodeID'] . '",'. "\n" .
				'"endNodeID" : "' . $row['endNodeID'] . '",'. "\n";		
		}
	}
}
//
// showNodes
//
// Show all of the nodes (by calling showNode for each).
//
function showNodes($storyID) {
	global $db, $table, $message, $json;
	$rows = $db->select("*", "nodes","storyID=$storyID");
	// Check for database errors.
	if ($db->errorCode)
		$message = "An error occurred: " . $db->errorMsg . "\n";
	else {	// no errors
		if ($db->numRows == 0) 
			$message = "No nodes found.";
		elseif ($db->numRows == 1) 
			showNode($rows);
		else {
			$rowsOutput = 0;
			foreach($rows as $row) {
				showNode($row);
				$rowsOutput = $rowsOutput + 1;
				if ($rowsOutput != $db->numRows)
					$json = $json .  ",\n";
				else
					$json = $json .  "\n";
			}
		}
	}
}
//
// showNode
//
// Build the JSON for a single node.
//
function showNode($row) {
	global $json;
	$json = $json .  '{"id" : "' . $row['id'] . '",'. "\n" .
		'"name" : "' . $row['name'] . '",'. "\n" .
		'"x" : "' . $row['x'] . '",'. "\n" .
		'"y" : "' . $row['y'] . '",'. "\n" .
		'"text" : "' . $row['text'] . '",'. "\n" .
		'"subtext" : "' . $row['subtext'] . '"}';
}
//
// showEdges
//
// Show all of the edges (by calling showEdge for each).
//
function showEdges($storyID) {
	global $db, $table, $message, $json;
	$rows = $db->select("*", "edges", "storyID=$storyID");
	// Check for database errors.
	if ($db->errorCode)
		$message = "An error occurred: " . $db->errorMsg . "\n";
	else {	// no errors
		if ($db->numRows == 0) 
			$message = "No nodes found.";
		elseif ($db->numRows == 1) 
			showEdge($rows);
		else {
			$rowsOutput = 0;
			foreach($rows as $row) {
				showEdge($row);
				$rowsOutput = $rowsOutput + 1;
				if ($rowsOutput != $db->numRows)
					$json = $json .  ",\n";
				else
					$json = $json .  "\n";
			}
		}
	}
}
//
// showEdge
//
// Build the JSON for a single edge.
//
function showEdge($row) {
	global $json;
	$json = $json .  '{"id" : "' . $row['id'] . '",'. "\n" .
		'"fromNodeID" : "' . $row['fromNodeID'] . '",'. "\n" .
		'"toNodeID" : "' . $row['toNodeID'] . '"}';
}
//
// Direct the building of the JSON
// data that includes story, node and
// edge information.
//
$json = $json .  "{";
$storyID = $_POST['storyID'];
showStory($storyID);
$json = $json .  '"nodes" : [' . "\n";
showNodes($storyID);
$json = $json .  "],\n";
$json = $json .  '"edges" : [' . "\n";
showEdges($storyID);
$json = $json .  "]\n";
$json = $json .  "}";
echo str_replace("\\'", "'", $json);
?>
