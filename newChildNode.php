<?php
//
// newChildNode.php
// Written by: Dr. David Brown
//
// Creates a new child node with associated edge
// from an existing node. It returns the recordID
// of the inserted record or 0 if unsuccessful.
// 
require_once 'includes/global.inc.php';

// Check to see if they're logged in
if(!isset($_SESSION['logged_in'])) {
	exit("User not authorized.");
}
$message = "";
//
// insertNode
//
// Insert a node using the current POST data.
//
// storyID - the ID of the story for which the node is being created
// x - x location of node to be created
// y - y location of node to be created
//
function insertNode() {
	global $db, $recordID, $data, $message;
	$data['storyID'] = $_POST['storyID'];
	$data['name'] = "untitled";
	$data['x'] = $_POST['x'];
	$data['y'] = $_POST['y'];
	$data['ownerID'] = $_POST['ownerID'];
	$recordID = $db->insert($data, "nodes");
	if ($db->errorCode) {
        $message = $db->errorMsg;
		$recordID = 0;
	}
}
//
// insertEdge
//
// Insert an edge to the new node.
//
function insertEdge() {
	global $db, $recordID, $data, $message;
	$data=[];
	$data['fromNodeID'] = $_POST['fromNodeID'];
	$data['storyID'] = $_POST['storyID'];
	$data['ownerID'] = $_POST['ownerID'];
	$data['toNodeID'] = $recordID;
	$db->insert($data, "edges");
	if ($db->errorCode) {
        $message = $db->errorMsg;
		$recordID = 0;
	}
}
//
// Create the node and edge and return
// the recordID of the inserted record.
//
insertNode();
if ($recordID != 0) {
	insertEdge();
}
echo $recordID;
?>