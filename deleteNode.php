<?php
//
// deleteNode.php
// Written by: Dr. David Brown
//
// Handles node deletion for leaf nodes.
// Also deletes incoming edges to the
// deleted node.
//
require_once 'includes/global.inc.php';

// Check to see if they're logged in
if(!isset($_SESSION['logged_in'])) {
	exit("User not authorized.");
}

//
// deleteNode
//
// Delete the current record.
//
function deleteNode() {
	global $db, $recordID, $message;
	$recordID = $_POST["id"];
	$storyID = $_POST["storyID"];
	//check to see if the node has children. If so, don't allow
	// the delete.
	$db->select("id","edges","fromNodeID = '" . $recordID . "'");
	if ($db->errorCode) {
		$message= $db->errorMsg;
		return;
	}
	if ($db->numRows != 0) {
		$message = "Can only delete leaf nodes.";
		return;
	}
	$result = $db->delete("edges","toNodeID = $recordID");
	if ($db->errorCode) {
		$message = $db->errorMsg;
		return;
	}
	$result = $db->delete("nodes","id = $recordID");
	if ($db->errorCode) {
		$message = $db->errorMsg;
		return;
	}
	$db->select("name","nodes","storyID = $storyID");
	if ($db->numRows == 0) {
		$result = $db->delete("stories","id = $storyID");
		if ($db->errorCode) {
			$message = $db->errorMsg;
			return;
		}
		else
			$message = "Story Deleted";
	}
	else
		$message = "Success";
}

deleteNode();
echo $message;
?>