<?php
  function showNavbar($user,$active = "") {
    echo '	<nav class="navbar navbar-default navbar-fixed-top" role="navigation">' . "\n";
    echo '	  <!-- Brand and toggle get grouped for better mobile display -->' . "\n";
    echo '	  <div class="navbar-header">' . "\n";
    echo '		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">' . "\n";
    echo '		  <span class="sr-only">Toggle navigation</span>' . "\n";
    echo '		  <span class="icon-bar"></span>' . "\n";
    echo '		  <span class="icon-bar"></span>' . "\n";
    echo '		  <span class="icon-bar"></span>' . "\n";
    echo '		</button>' . "\n";
    echo '		<a class="navbar-brand" href="index.php">StoryTrees</a>' . "\n";
    echo '	  </div>' . "\n";
    echo '		' . "\n";
    echo '		  <!-- Collect the nav links, forms, and other content for toggling -->' . "\n";
    echo '		  <div class="collapse navbar-collapse navbar-ex1-collapse">' . "\n";
    echo '			<ul class="nav navbar-nav">' . "\n";
	 echo '			  <li><a href="stories.php">Stories</a></li>' . "\n";
    echo '			  <li class="disabled"><a href="characters.php">Characters</a></li>' . "\n";
    echo '			  <li class="disabled"><a href="symbols.php">Symbols</a></li>' . "\n";
    //if ($user->userPriv == 'A') {
    //	echo '			  <li><a href="users.php">Users</a></li>' . "\n";
    //}
    echo '			</ul>' . "\n";
    echo '			<ul class="nav navbar-nav navbar-right">' . "\n";
    echo '			  <li style="margin-top: 15; margin-right: 10">Welcome ' . "$user->firstName" . '!</li>' . "\n";
    echo '			  <li><a class="btn btn-default" href="logout.php">Log Out</a></li>' . "\n";
    echo '			</ul>' . "\n";
    echo '		  </div><!-- /.navbar-collapse -->' . "\n";
    echo '		</nav>' . "\n";
  }
?>
