<?php
//
// global.inc.php
//
//	Start the session
session_name("StoryTrees");
session_start();

// Includes
require_once 'classes/User.class.php';
require_once 'classes/UserTools.class.php';
require_once 'classes/DB.class.php';
require_once 'includes/navbar.inc.php';
require_once 'includes/utils.inc.php';

// Open the database connection
$db = new DB();
// Create UserTools Object
$userTools = new UserTools($db);

// If someone is logged in, set $userID
// and $user globals.
if(isset($_SESSION['logged_in'])) {
	$userID = $_SESSION["userID"];
	$user = $userTools->get($userID);
}
else {
	$userID = "";
	$user = null;
}
?>