<?php 
// user.php
// Written by: Dr. David Brown
//
// Add, update or delete a user.
//
require_once 'includes/global.inc.php';

//check to see if they're logged in
if(!isset($_SESSION['logged_in'])) {
	header("Location: login.php");
}

// This function is only available to administrators.
if ($user->userPriv != 'A') {
	header("Location: login.php");
}

// Database table name to work with.
$table = "users";
// ID of current record
$recordID = "";
// Data maintained for the record
$data = array(
	"username" => "",
	"password" => "",
	"email" => "",
	"firstName" => "",
	"lastName" => ""
);
// Informational message printed at bottom of page.
$message = "";
// Used to verify password.
$passwordConfirm = "";
//
// Determine which function has been requested.
//
if(isset($_POST['SHOW_RECORD'])) 
	showRecord();
else if(isset($_POST['UPDATE_RECORD']))
	updateRecord();
else if(isset($_POST['DELETE_RECORD'])) 
	deleteRecord();
else if(isset($_POST['ADD_RECORD'])) 
	insertRecord();
//
// showRecord
//
// Show one record using the POST record ID.
//
function showRecord() {
	global $db, $table, $recordID, $data, $message;
	$recordID = $_POST['recordID'];
	$result = $db->select("*",$table,"id = $recordID");
	if ($db->errorCode)
		$message = $db->errorMsg;
	else 
		setFields($result);
}
//
// insertRecord
//
// Insert a record using the current POST data.
//
function insertRecord() {
	global $db, $table, $recordID, $data, $passwordConfirm, $message, $userTools;
	getPost();
	$success = true;		
	//check to see if user name already exists
	if($userTools->userNameExists($data['username'])) {
		$message .= "That username is already taken.<br/> \n\r";
		$success = false;
	}
	//check to see if passwords match
	if($data['password'] != $passwordConfirm) {
		$message .= "Passwords do not match.<br/> \n\r";
		$success = false;
	}
	// If no problems, add the record.
	if($success) {
		$password = $data['password'];
		$data['password'] = md5($password);
		$recordID = $db->insert($data, $table);
		$data['password'] = $password;
		if ($db->errorCode) {
			$message = $db->errorMsg;
			$recordID = "";
		}
		else
			$message = $data['firstName'] . " " . $data['lastName'] . " successfully added.";
	}
}
//
// updateRecord
//
// Update a record using the current POST data.
//
function updateRecord() {
	global $db, $table, $recordID, $data, $message;
	getPost();
	$password = $data['password'];
	$data['password'] = md5($password);
	$result = $db->update($data,$table,"id = $recordID");
	$data['password'] = $password;
	if ($db->errorCode)
		$message = $db->errorMsg;
	else
		$message = "Record successfully updated.";
}
//
// deleteRecord
//
// Delete the current record.
//
function deleteRecord() {
	global $db, $table, $recordID, $message;
	$recordID = $_POST["recordID"];
	$result = $db->delete($table,"id = $recordID");
	if ($db->errorCode)
		$message = $db->errorMsg;
	else {
		$recordID = "";
		$message = "Record successfully deleted.";
	}
}
//
// getPost
//
//	Load the $data structure with the current POST data.
//
function getPost() {
	global $db, $recordID, $data, $passwordConfirm;
	$recordID = $_POST['recordID'];
	$data['username'] = mysqli_real_escape_string($db->connection, $_POST['username']);
	$data['password'] = mysqli_real_escape_string($db->connection, $_POST['password']);
	$passwordConfirm = mysqli_real_escape_string($db->connection, $_POST['passwordConfirm']);
	$data['email'] = mysqli_real_escape_string($db->connection, $_POST['email']);
	$data['firstName'] = mysqli_real_escape_string($db->connection, $_POST['firstName']);
	$data['lastName'] = mysqli_real_escape_string($db->connection, $_POST['lastName']);
}
//
// setFields
//
// Set the form fields using the $data structure.
//
function setFields($result) {
	global $data, $passwordConfirm;
	$data['username'] = $result['username'];
	$data['password'] = "";
	$passwordConfirm = "";
	$data['email'] = $result['email'];
	$data['firstName'] = $result['firstName'];
	$data['lastName'] = $result['lastName'];
}
?>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="Add, update or delete stories." content="">
	<meta name="Dr. Brown" content="">
	<link rel="shortcut icon" href="images/favicon.png">

	<title>User</title>

	<!-- Bootstrap core CSS -->
	<link href="css/bootstrap.css" rel="stylesheet">
	<link href="css/custom.css" rel="stylesheet">

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
      <script src="../../assets/js/html5shiv.js"></script>
      <script src="../../assets/js/respond.min.js"></script>
    <![endif]-->
</head>
<body>
	<?php showNavbar($user);?>
	<div class="container">
		<h2>User:</h2>
		<form action="user.php" method="post">	
		 <?php
			  echo '<input type="hidden" name="recordID" id="recordID" value="' . $recordID .
					'">';
		 ?>
		<input type="text" class="form-control" placeholder="PSCC User Name" value="<?php echo $data['username']; ?>" autofocus name="username" required/><br>
		<input type="text" class="form-control" placeholder="First Name" value="<?php echo $data['firstName']; ?>" name="firstName" required/><br>
		<input type="text" class="form-control" placeholder="Last Name" value="<?php echo $data['lastName']; ?>" name="lastName" required/><br>
		<input type="password" class="form-control" placeholder="Password" value="<?php echo $data['password']; ?>" name="password" required/>
		<input type="password" class="form-control" placeholder="Password (confirm)" value="<?php echo $passwordConfirm; ?>" name="passwordConfirm" required/>
		<input type="text" class="form-control" placeholder="Email address" value="<?php echo $data['email']; ?>" name="email" required/><br>
		<?php
			// Show the ADD button if this is a new record.
			if ($recordID == "")
				echo '<button type="submit" class="btn btn-lg btn-primary btn-block"' . 
					'value="1" name="ADD_RECORD" />Add</button>';
			// If this is an existing record show the UPDATE and DELETE buttons.
			else {
				echo '<button type="submit" class="btn btn-lg btn-primary btn-block"' .
					'value="1" name="UPDATE_RECORD" />Update</button>';
				echo '<button type="submit" class="btn btn-lg btn-primary btn-block"' .
					'value="1" name="DELETE_RECORD" />Delete</button>';
			}
		?>
	</form>
	<?php print $message; ?>
</body>
</html>
