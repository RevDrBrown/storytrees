<?php
//
// logout.php
// Written by: Dr. David Brown
//
// Log a user out of the system utilizing the logout
// method in UserTools.
//
require_once 'includes/global.inc.php';

$userTools->logout();

header("Location: login.php");
?>