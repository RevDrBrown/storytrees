<?php
require_once 'DB.class.php';
//
// User
//
// DB support class for the user table.
//
class User {

	// Properties
	public $id;
	public $username;
	public $hashedPassword;
	public $email;
	public $joinDate;
	public $firstName;
	public $lastName;
	public $link;
	public $blog;
	public $db;
	
	//Constructor is called whenever a new object is created.
	//Takes an associative array with the DB row as an argument.
	function __construct($data, $db = null) {
		$this->id = (isset($data['id'])) ? $data['id'] : "";
		$this->username = stripslashes((isset($data['username'])) ? $data['username'] : "");
		$this->hashedPassword = (isset($data['password'])) ? $data['password'] : "";
		$this->email = stripslashes((isset($data['email'])) ? $data['email'] : "");
		$this->joinDate = (isset($data['join_date'])) ? $data['join_date'] : "";
		$this->firstName = stripslashes((isset($data['firstName'])) ? $data['firstName'] : "");
		$this->lastName = stripslashes((isset($data['lastName'])) ? $data['lastName'] : "");
		$this->userPriv = (isset($data['userPriv'])) ? $data['userPriv'] : "";
		$this->link = stripslashes((isset($data['link'])) ? $data['link'] : "");
		$this->blog = stripslashes((isset($data['blog'])) ? $data['blog'] : "");
	}
}

?>