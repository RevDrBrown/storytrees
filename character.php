<?php 
//
// character.php
// Written by: Dr. David Brown
//
// Handle insertion, modification and
// deletion of characters.
//
require_once 'includes/global.inc.php';

//check to see if they're logged in
if(!isset($_SESSION['logged_in'])) {
	header("Location: login.php");
}

// Database table name to work with.
$table = "characters";
// ID of current record
$recordID = "";
// Data maintained for the record
$data = array(
	"name" => "",
	"text" => ""
);
// Informational message printed at end of page.
$message = "";

//
// Determine which function has been requested.
//
if(isset($_POST['SHOW_RECORD'])) 
	showRecord();
else if(isset($_POST['UPDATE_RECORD']))
	updateRecord();
else if(isset($_POST['DELETE_RECORD'])) 
	deleteRecord();
else if(isset($_POST['ADD_RECORD'])) 
	insertRecord();
//
// showRecord
//
// Show one record using the POST record ID.
//
function showRecord() {
	global $db, $table, $recordID, $data, $message;
	$recordID = $_POST['recordID'];
	$result = $db->select("*",$table,"id = $recordID");
	if ($db->errorCode)
		$message = $db->errorMsg;
	else 
		setFields($result);
}
//
// insertRecord
//
// Insert a record using the current POST data.
//
function insertRecord() {
	global $db, $table, $recordID, $data, $message;
	getPost();
	$success = true;		
	//check to see if character name already exists
	$db->select("name",$table,"name = '" . $data['name'] . "'");
	if ($db->numRows != 0) {
		$message .= "That character name is already taken.<br/> \n\r";
		$success = false;
	}
	// If no problems, add the record.
	if($success) {
		$recordID = $db->insert($data, $table);
		if ($db->errorCode) {
			$message = $db->errorMsg;
			$recordID = "";
		}
		else
			$message = $data['name'] . " successfully added.";
	}
}
//
// updateRecord
//
// Update a record using the current POST data.
//
function updateRecord() {
	global $db, $table, $recordID, $data, $message;
	getPost();
	$result = $db->update($data,$table,"id = $recordID");
	if ($db->errorCode)
		$message = $db->errorMsg;
	else
		$message = "Record successfully updated.";
}
//
// deleteRecord
//
// Delete the current record.
//
function deleteRecord() {
	global $db, $table, $recordID, $message;
	$recordID = $_POST["recordID"];
	$result = $db->delete($table,"id = $recordID");
	if ($db->errorCode)
		$message = $db->errorMsg;
	else {
		$recordID = "";
		$message = "Record successfully deleted.";
	}
}
//
// getPost
//
//	Load the $data structure with the current POST data.
//
function getPost() {
	global $db, $recordID, $data;
	$recordID = $_POST['recordID'];
	$data['name'] = mysqli_real_escape_string($db->connection, $_POST['name']);
	$data['text'] = mysqli_real_escape_string($db->connection, $_POST['text']);
}
//
// setFields
//
// Set the form fields using the $data structure.
//
function setFields($result) {
	global $data;
	$data['name'] = $result['name'];
	$data['text'] = $result['text'];
}
?>
<html>

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="Add, update or delete characters." content="">
	<meta name="Dr. Brown" content="">
	<link rel="shortcut icon" href="images/favicon.png">

	<title>Character</title>

	<!-- Bootstrap core CSS -->
	<link href="css/bootstrap.css" rel="stylesheet">
	<link href="css/custom.css" rel="stylesheet">

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
      <script src="../../assets/js/html5shiv.js"></script>
      <script src="../../assets/js/respond.min.js"></script>
    <![endif]-->
</head>

<body>
	<?php showNavbar($user);?>
	<div class="container">
		<h2>Character:</h2>
		<form action="character.php" method="post">
			<?php echo '<input type="hidden" name="recordID" id="recordID" value="' . $recordID . '">'; ?>
			<input type="text" class="form-control" placeholder="Character Name" value="<?php echo $data['name']; ?>" autofocus name="name" required/>
			<br>
			<textarea class="form-control" rows="3" name="text"><?php echo $data['text']; ?></textarea>
			<br>	
			<?php
				// Show the ADD button if this is a new record.
				if ($recordID == "")
					echo '<button type="submit" class="btn btn-lg btn-primary btn-block"' . 
						'value="1" name="ADD_RECORD" />Add</button>';
				// If this is an existing record show the UPDATE and DELETE buttons.
				else {
					echo '<button type="submit" class="btn btn-lg btn-primary btn-block"' .
						'value="1" name="UPDATE_RECORD" />Update</button>';
					echo '<button type="submit" class="btn btn-lg btn-primary btn-block"' .
						'value="1" name="DELETE_RECORD" />Delete</button>';
				}
			?>
	</form>
	<?php print $message; ?>
</body>
</html>
